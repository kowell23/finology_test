#!/usr/bin/ruby

require 'sqlite3'

begin
    db = SQLite3::Database.open "test.db"
    db.execute "CREATE TABLE IF NOT EXISTS Products(Id INTEGER PRIMARY KEY, 
        Name TEXT, Price TEXT, Description TEXT, MoreInformation TEXT)"
rescue SQLite3::Exception => e 
    puts "Exception occurred"
    puts e
ensure
    db.close if db
end
