# A web crawler in Ruby
#!/usr/bin/ruby
# encoding: utf-8

require 'nokogiri'
require 'open-uri'
require 'sqlite3'

require_relative 'controllers/products_controllers'

url = "https://magento-test.finology.com.my/breathe-easy-tank.html"
doc = Nokogiri::HTML(URI.open(url))

description_details = []
extra_information_label = []
extra_information_data = []
new_extra_information_data = []
all_products = []
preview_product = []
related_products = []


# ========== preview product ======== #
# get title
name = doc.css('div h1').first.content.strip
preview_product.push(name)

# get price
price = doc.css('span .price').first.content
preview_product.push(price)

# get details
doc.css('div .product.attribute.description .value').each do |link|
    description_details.push(link.content)
end

description_txt = SQLite3::Database.quote(description_details.join(" "))
preview_product.push(description_txt)

# get extra information
doc.css('tbody .label').each do |link|
    extra_information_label.push("%s:" % [link.content])
end

doc.css('tbody .data').each do |link|
    extra_information_data.push(link.content)
end
combined_extra_information = extra_information_label.zip(extra_information_data)
combined_extra_information.each do |combine|
    new_extra_information_data.push(combine.join(" "))
end
preview_product.push(new_extra_information_data.join(" | "))
all_products.push(preview_product)
# ========================================================= #

# ========== related products ======== #
related_product_name = []
related_product_price = []
related_product_description = []
related_product_extra_information = []
related_products_total = doc.css('.product.name.product-item-name').length
doc.css('.product.name.product-item-name').each do |product_name|
    related_product_name.push(product_name.content.strip)
end
price_index = 0
doc.css('span .price').each do |price|
    if price_index > 0
        related_product_price.push(price.content)
    end

    price_index += 1
end

for i in 0..related_products_total.to_i
    related_product_description.push("No Description")
    related_product_extra_information.push("No Extra Information")
end

all_products = all_products + related_product_name.zip(related_product_price, related_product_description, related_product_extra_information)
# ==================================== #

all_products.each do |p|
    begin
        db = SQLite3::Database.open "test.db"
        # check existing product name
        stm = db.prepare "SELECT * FROM Products WHERE Name=?"
        stm.bind_param 1, p[0].to_s
        rs = stm.execute
        row = rs.next
        unless row
            db.execute("INSERT INTO Products(Name, Price, Description, MoreInformation) VALUES ('#{p[0]}', '#{p[1]}', '#{p[2]}', '#{p[3]}')")
        end
    rescue SQLite3::Exception => e 
        puts "Exception occurred"
        puts e
    ensure
        stm.close if stm
        db.close if db
    end
end

products = Products.new
puts products.get_all_products
