#!/usr/bin/ruby

require 'sqlite3'

class Products
    def initialize()
    end

    def get_all_products
        begin
            db = SQLite3::Database.open "test.db"
            
            datas = []
            stm = db.prepare "SELECT * FROM Products" 
            rs = stm.execute 
            rs.each do |row|
                datas.push(row)
            end

            puts datas
        rescue SQLite3::Exception => e 
            puts "Exception occurred"
            puts e
        ensure
            stm.close if stm
            db.close if db
        end
    end
end
